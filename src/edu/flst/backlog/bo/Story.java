package edu.flst.backlog.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.ToStringBuilder;


public class Story implements Serializable, Comparable<Story> {

	private static final long serialVersionUID = -4589061225339520890L;

	private int id;
	@NotNull
	@Size(max=50,min=2)
	private String label;
	private String comment;
	@NotNull
	private Date createdDate;
	@NotNull
	private Date modifiedDate;
	@NotNull
	private List<User> users = new ArrayList<User>();
	@NotNull
	private Status status;
	@NotNull
	private List<Component> components = new ArrayList<Component>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@NotNull
	@Size(max=50,min=2)
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	@NotNull
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@NotNull
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setComponents(List<Component> components) {
		this.components = components;
	}

	public List<Component> getComponents() {
		return components;
	}

	public void addComponent(Component component){
		components.add(component);
	}
	
	public void updateComponent(Component component){
		users.remove(component);
		components.add(component);
	}

	public void removeComponent(Component component){
		components.remove(component);
	}
	
	
	@NotNull
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}


	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<User> getUsers() {
		return users;
	}

	public void addUser(User user){
		users.add(user);
	}
	
	public void updateUser(User user){
		users.remove(user);
		users.add(user);
	}

	public void removeUser(User user){
		users.remove(user);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Story other = (Story) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	@Override
	public int compareTo(Story arg0) {
		// TODO Auto-generated method stub
		return id - arg0.getId();
	}
}
