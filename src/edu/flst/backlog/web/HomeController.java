package edu.flst.backlog.web;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import edu.flst.backlog.bo.Component;
import edu.flst.backlog.bo.Job;
import edu.flst.backlog.bo.Status;
import edu.flst.backlog.bo.Story;
import edu.flst.backlog.bo.User;
import edu.flst.backlog.service.BacklogService;


@Controller
public class HomeController {
	
	final static Logger logger = Logger.getLogger(HomeController.class);
	
	@Autowired private BacklogService backlogService;

	@RequestMapping(value="/home.do", method=RequestMethod.GET)
	public ModelAndView home(HttpServletRequest req, HttpServletResponse resp){	
	 
		logger.info("Info : Connexion Home page" );
		
		String fromDemo = req.getParameter("fromDemo");
		
		logger.info("From demo " +fromDemo );
		
		/* On regroupe les stories par status */
		List<Story> openStories 		= new ArrayList<Story>();
		List<Story> inProgressStories 	= new ArrayList<Story>();
		List<Story> doneStories 		= new ArrayList<Story>();
		List<Story> laststories 		= new ArrayList<Story>();
		List<Story> allStories 			= new ArrayList<Story>();
		
		// Toutes les taches
		try{
			allStories = backlogService.listStories();
		}
		catch(ClassCastException e){
			logger.error("Erreur pour recuperer la liste des taches : " + e.getMessage() );
		}
		
		
		for (Story story : allStories){
	    	
	    	if ( story.getStatus().name().equalsIgnoreCase("OPEN") ){
	    		openStories.add(story);
	    		logger.info("Info : Ajout d'une tache OPEN" );
	    	}
	    	else if(story.getStatus().name().equals("IN_PROGRESS") ){
	    		inProgressStories.add(story);
	    		logger.info("Info : Ajout d'une tache IN_PROGRESS" );
	    	}
	    	else if(story.getStatus().name().equalsIgnoreCase("DONE") ){
	    		doneStories.add(story);
	    		logger.info("Info : Ajout d'une tache DONE" );
	    	}
	    }
		/* Fin du regroupement */
		
		/* On trie les listes par date d�croissant de modification */
		logger.info("Info : Trie des listse par date d�croissant de date de modification" );
		
		Collections.sort(openStories, new Comparator<Story>() {
		    public int compare(Story open1, Story open2) {
		        return open2.getModifiedDate().compareTo(open1.getModifiedDate());
		    }
		});
		
		Collections.sort(inProgressStories, new Comparator<Story>() {
		    public int compare(Story inProgress1, Story inProgress2) {
		        return inProgress2.getModifiedDate().compareTo(inProgress1.getModifiedDate());
		    }
		});
		
		Collections.sort(doneStories, new Comparator<Story>() {
		    public int compare(Story done1, Story done2) {
		        return done2.getModifiedDate().compareTo(done1.getModifiedDate());
		    }
		});
		
		Collections.sort(allStories, new Comparator<Story>() {
		    public int compare(Story s1, Story s2) {
		        return s2.getModifiedDate().compareTo(s1.getModifiedDate());
		    }
		});
		/* Fin des tris */
	        
		// On garde que les plus recentes
		if(allStories.size() > 3){
			logger.info("Info : Pour les dernieres taches, on garde que les 3 plus recentes" );
			laststories = allStories.subList(0, 3);			
		}
		else{
			laststories = allStories;
		}
		
		// Calcul du % de tache accomplie = (nb de taches DONE / nb de taches Total) * 100
		int pourcentageAccompliArrondi = 0 ;
		try{
			BigDecimal totalDone = new BigDecimal(doneStories.size()) ;
			int totalStories  	= doneStories.size() + inProgressStories.size() + openStories.size() ;
			BigDecimal totalSotiresBig = new BigDecimal(totalStories);
			
			BigDecimal resultat = totalDone.divide(totalSotiresBig, 2, RoundingMode.CEILING);
			BigDecimal pourcentageCalcul = resultat.multiply(new BigDecimal("100"));
			pourcentageAccompliArrondi = pourcentageCalcul.intValue();
		}
		catch(ArithmeticException e){
			// On laisse a 0
			logger.error("Impossible de calculer le % de taches done, il n'y a peut etre aucune tache : " + e.getMessage() );
		}

		/* On renvoie la vue */
		try{
		    ModelAndView mNv = new ModelAndView("home");
				mNv.addObject("lastStories", laststories);
				mNv.addObject("openStories", openStories);
				mNv.addObject("inProgressStories", inProgressStories);
				mNv.addObject("doneStories", doneStories);
				mNv.addObject("pourcentageAccompli", pourcentageAccompliArrondi);
				mNv.addObject("fromDemo", fromDemo);

			return mNv;
		}
		catch(ClassCastException e){
			logger.error("Erreur pour renvoyer la vue : " + e.getMessage() );
			return null;
		}
		
	}
	
	
	@RequestMapping(value="/home/majdiv.do")
	public void majDivHome(HttpServletRequest request, HttpServletResponse response) throws IOException{ 
			
		String result = "<table class='table table-bordered'><tr><th class='backlog'><center>TO DO</center></th><th class='backlog'><center>IN PROGRESS</center></th><th class='backlog'><center>DONE</center></th></tr><tr>";
		
		// Toutes les taches
		List<Story> allStories 			= new ArrayList<Story>();
		try{
			allStories = backlogService.listStories();
		}
		catch(ClassCastException e){
			logger.error("Erreur pour recuperer la liste des taches : " + e.getMessage() );
		}
		
		
		/* On regroupe les stories par status */
		List<Story> openStories 		= new ArrayList<Story>();
		List<Story> inProgressStories 	= new ArrayList<Story>();
		List<Story> doneStories 		= new ArrayList<Story>();
		
		for (Story story : allStories){
	    	
	    	if ( story.getStatus().name().equalsIgnoreCase("OPEN") ){
	    		openStories.add(story);
	    	}
	    	else if(story.getStatus().name().equals("IN_PROGRESS") ){
	    		inProgressStories.add(story);
	    	}
	    	else if(story.getStatus().name().equalsIgnoreCase("DONE") ){
	    		doneStories.add(story);
	    	}
	    }
		/* Fin du regroupement */
		
		/* On trie les liste par date d�croissante de modification */
		Collections.sort(openStories, new Comparator<Story>() {
		    public int compare(Story open1, Story open2) {
		        return open2.getModifiedDate().compareTo(open1.getModifiedDate());
		    }
		});
		
		Collections.sort(inProgressStories, new Comparator<Story>() {
		    public int compare(Story inProgress1, Story inProgress2) {
		        return inProgress2.getModifiedDate().compareTo(inProgress1.getModifiedDate());
		    }
		});
		
		Collections.sort(doneStories, new Comparator<Story>() {
		    public int compare(Story done1, Story done2) {
		        return done2.getModifiedDate().compareTo(done1.getModifiedDate());
		    }
		});
		/* Fin des tris */
		
		// Calcul du % de tache accomplie = (nb de taches DONE / nb de taches Total) * 100
		int pourcentageAccompliArrondi = 0 ;
		try{
			BigDecimal totalDone = new BigDecimal(doneStories.size()) ;
			int totalStories  	= doneStories.size() + inProgressStories.size() + openStories.size() ;
			BigDecimal totalSotiresBig = new BigDecimal(totalStories);
			
			BigDecimal resultat = totalDone.divide(totalSotiresBig, 2, RoundingMode.CEILING);
			BigDecimal pourcentageCalcul = resultat.multiply(new BigDecimal("100"));
			pourcentageAccompliArrondi = pourcentageCalcul.intValue();
		}
		catch(ArithmeticException aex){
			// On laisse a 0
		}
		
		// On construit la progress bar
		result += "<tr><td colspan='3'><div class='progress'>";
		result += "<div class='progress-bar' role='progressbar' aria-valuenow='"+pourcentageAccompliArrondi+"' aria-valuemin='0' aria-valuemax='100' style='width: "+pourcentageAccompliArrondi+"%;'>";
		result += ""+pourcentageAccompliArrondi+"%";
		result +=  "</div></div></td></tr>";
		
		// Pour formater les dates
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
		
		/* On construit le tableau */
		result += 	"<td>";
		if(!(openStories.isEmpty())){	
			for (Story story : openStories){
				result +=		"<a href='#'>";
				result +=			"<div class='alert alert-backlog alert-danger col-sm-5' id='"+story.getId()+"' data-toggle='popover' data-trigger='hover' title='Commentaire' data-content='"+story.getComment()+"' data-container='body' data-placement='right'>";
				result +=				"<center><u><strong>"+story.getLabel()+"</strong></u></center><br>";
				result +=				"<strong>Cr�ation : </strong><br>"+df.format(story.getCreatedDate())+"<br>";
				result +=				"<strong>Derni�re modif. : </strong>"+df.format(story.getModifiedDate())+"<br><br>";
				result +=		"<strong>Utilisateur(s) : </strong><br>";
								for (User user : story.getUsers()){
									result +=		"<ul>"+user.getFirstName()+" "+user.getLastName()+"</ul>";
								}
				result +=		"<br>";
				result += 				"<strong>Composant(s) : </strong><br>";
								for (Component composant : story.getComponents()){
									result +=		"<ul>"+composant.getLabel()+"</ul>";
								}
				result +=			"</div>";
				result +=		"</a>";
			}
		}
		result += 	"</td>";
		
		result += 	"<td>";
		if(!(inProgressStories.isEmpty())){	
			for (Story open : inProgressStories){
				result +=		"<a href='#'>";
				result +=			"<div class='alert alert-backlog alert-warning col-sm-5' id='"+open.getId()+"' data-toggle='popover' data-trigger='hover' title='Commentaire' data-content='"+open.getComment()+"' data-container='body' data-placement='right'>";
				result +=				"<center><u><strong>"+open.getLabel()+"</strong></u></center><br>";
				result +=				"<strong>Cr�ation : </strong><br>"+df.format(open.getCreatedDate())+"<br>";
				result +=				"<strong>Derni�re modif. : </strong>"+df.format(open.getModifiedDate())+"<br><br>";
				result +=				"<strong>Utilisateur(s) : </strong><br>";
										for (User user : open.getUsers()){
											result +=		"<ul>"+user.getFirstName()+" "+user.getLastName()+"</ul>";
										}
				result +=				"<br>";
				result += 				"<strong>Composant(s) : </strong><br>";
										for (Component composant : open.getComponents()){
											result +=		"<ul>"+composant.getLabel()+"</ul>";
										}
				result +=			"</div>";
				result +=		"</a>";
			}
		}
		result += 	"</td>";
		
		result += 	"<td>";
		if(!(doneStories.isEmpty())){	
			for (Story done : doneStories){
				result +=		"<a href='#'>";
				result +=			"<div class='alert alert-backlog alert-success col-sm-5' id='"+done.getId()+"' data-toggle='popover' data-trigger='hover' title='Commentaire' data-content='"+done.getComment()+"' data-container='body' data-placement='left'>";
				result +=				"<center><u><strong>"+done.getLabel()+"</strong></u></center><br>";
				result +=				"<strong>Cr�ation : </strong><br>"+df.format(done.getCreatedDate())+"<br>";
				result +=				"<strong>Derni�re modif. : </strong>"+df.format(done.getModifiedDate())+"<br><br>";
				result +=				"<strong>Utilisateur(s) : </strong><br>";
				for (User user : done.getUsers()){
					result +=			"<ul>"+user.getFirstName()+" "+user.getLastName()+"</ul>";
				}
				result +=				"<br>";
				result += 				"<strong>Composant(s) : </strong><br>";
										for (Component composant : done.getComponents()){
											result +=		"<ul>"+composant.getLabel()+"</ul>";
										}
				result +=			"</div>";
				result +=		"</a>";
			}
		}
		result += 	"</td>";
		 
		result += 	"</tr></table>";
		
		try{
			response.setContentType("text/html");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(result);
		}
		catch(ClassCastException e){
			logger.error("Erreur dans le renvoie de la r�ponse pour la mise a jour de la div backlog : " + e.getMessage() );
		}
	}
	
	@RequestMapping(value="/demo.do", method=RequestMethod.GET)
	public void demo(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		// On ajoute 15 users
		for(int i = 0; i < 15; i++)
		{
			User user = new User();
			user.setFirstName("Prenom"+i);
			user.setLastName("Nom"+i);
			if (i<6){
				user.setJob(Job.ANALYST);
			}
			else if (i > 5 && i < 10){
				user.setJob(Job.DEVELOPPER);
			}
			else{
				user.setJob(Job.PROJECT_MANAGER);
			}
			
			try{
				backlogService.createUser(user);
			}
			catch(ClassCastException e){
				logger.error("Impossible de cr�er le user de demo "+i+" (erreur backlogService) : " + e.getMessage() );
			}
			
		}
		
		// On ajoute 15 composants
		for(int i = 0; i < 15; i++)
		{
			Component composant = new Component();
			composant.setLabel("Label"+1);
			composant.setDescription("Description<br> Ceci est la description du composant "+i);
			
			User user = backlogService.getUser(i);
			composant.setOwner(user);
			
			try{
				backlogService.createComponent(composant);
			}
			catch(ClassCastException e){
				logger.error("Impossible de cr�er le composant de demo "+i+" (erreur backlogService) : " + e.getMessage() );
			}			
			
		}
		
		// On ajoute 10 taches, 4 - 2 - 4
		for(int i = 0; i < 11; i++)
		{
			Story story = new Story();
			story.setLabel("Label"+i);
			story.setComment("Commentaire de tache.<br>Ceci est le commentaire de la tache "+i+".<br>Un commentaire sur plusieurs lignes.");
			
			if(i < 4){
				story.setStatus(Status.OPEN);
				
				if(i < 3){
					for(int j = 0; j < 3; j++){
						User user = backlogService.getUser(j);
						story.addUser(user);
					}
					for(int k = 0; k < 2; k++){
						Component composant = backlogService.getComponent(k);
						story.addComponent(composant);
					}
				}
				else if(i > 2 && i < 5){
					for(int j = 4; j < 5; j++){
						User user = backlogService.getUser(j);
						story.addUser(user);
					}
					for(int k = 2; k < 4; k++){
						Component composant = backlogService.getComponent(k);
						story.addComponent(composant);
					}					
				}
					
			}
			else if(i > 3 && i < 6){
				story.setStatus(Status.IN_PROGRESS);
				
				User user = backlogService.getUser(10);
				story.addUser(user);
				
				Component composant = backlogService.getComponent(10);
				story.addComponent(composant);
				Component composant2 = backlogService.getComponent(11);
				story.addComponent(composant2);	
				
			}
			else{
				story.setStatus(Status.DONE);
				
				User user = backlogService.getUser(9);
				story.addUser(user);
				User user2 = backlogService.getUser(10);
				story.addUser(user2);
				
				Component composant = backlogService.getComponent(13);
				story.addComponent(composant);				
			}
			
			Date createdDate = new Date(); // Date du jour
			story.setCreatedDate(createdDate);
			story.setModifiedDate(createdDate);
			

			try{
				backlogService.createStory(story);
			}
			catch(ClassCastException e){
				logger.error("Impossible de cr�er la tache de demo "+i+" (erreur backlogService) : " + e.getMessage() );
			}	
			
		}
		
		/* On redirige vers /home.do pour revenir � la home page */
		StringBuffer url 	= request.getRequestURL();
		String uri 			= request.getRequestURI();
		String ctx 			= request.getContextPath();
		String baseurl 		= url.substring(0, url.length() - uri.length() + ctx.length()) + "/";
		
		response.sendRedirect(response.encodeRedirectURL(baseurl+"home.do?fromDemo=true"));
		
	}

}
