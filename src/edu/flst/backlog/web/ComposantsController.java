package edu.flst.backlog.web;

import java.util.ArrayList;
import java.util.List;

import edu.flst.backlog.bo.Component;
import edu.flst.backlog.bo.User;
import edu.flst.backlog.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ComposantsController {
	
	final static Logger logger = Logger.getLogger(ComposantsController.class);
	
	@Autowired private BacklogService backlogService;
	
	@RequestMapping(value="/composants.do", method=RequestMethod.GET)
	public ModelAndView composants(){

		List<Component> allComponents = new ArrayList<Component>();
		
		try{
			allComponents = backlogService.listComponents();
		}
		catch(ClassCastException e){
			logger.error("Erreur pour recuperer la liste des composant : " + e.getMessage() );
		}

		
	    ModelAndView mNv = new ModelAndView("composants");
			mNv.addObject("composants", allComponents);	


		return mNv;
		
	}
	
	@RequestMapping(value="/composants/new.do", method=RequestMethod.GET)
	public ModelAndView composantsNew(){
		
		ModelAndView mNv = new ModelAndView("composantsNew");
		
		List<User> allUsers = new ArrayList<User>();
		
		try{
			allUsers = backlogService.listUsers();
		}
		catch(ClassCastException e){
			logger.error("Erreur pour recuperer la liste des users : " + e.getMessage() );
		}
		
		mNv.addObject("users", allUsers);

		return mNv;
		
	}

	@RequestMapping(value="/composants/create.do", method=RequestMethod.POST)
	public ModelAndView usersCreate(HttpServletRequest req, HttpServletResponse resp){
		
		// On recupere les parametres
		String label = req.getParameter("label");
		String description = req.getParameter("description");
		String owner = req.getParameter("owner");
	
		// On cr�� un nouveau composant
		Component composant = new Component();
		composant.setLabel(label);
		composant.setDescription(description);
		
		User user = null;
			try{
				user = backlogService.getUser(Integer.parseInt(owner));
			}
			catch(ClassCastException e){
				logger.error("Erreur lors de la recuperation du user avec son id : " + e.getMessage() );
			}
		composant.setOwner(user);
		
		try{
			backlogService.createComponent(composant);
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la cr�ation du composant : " + e.getMessage() );
		}
		
		ModelAndView mNv = new ModelAndView("composantsActionSuccess");
		mNv.addObject("action", "Cr�ation");
		
		return mNv;
	}

	@RequestMapping(value="/composants/edit/{id}.do", method=RequestMethod.GET)
	public ModelAndView usersEdit(@PathVariable int id){
		ModelAndView mNv = new ModelAndView("composantsEdit");
		
		Component composant = backlogService.getComponent(id);
		
		List<User> allUsers = new ArrayList<User>();
		
		try{
			allUsers = backlogService.listUsers();
		}
		catch(ClassCastException e){
			logger.error("Erreur pour recuperer la liste des users : " + e.getMessage() );
		}
		
		mNv.addObject("composant", composant);
		mNv.addObject("users", allUsers);
		
		return mNv;
	}

	@RequestMapping(value="/composants/update.do", method=RequestMethod.POST)
	public ModelAndView usersUpdate(HttpServletRequest req, HttpServletResponse resp){
		
		// On recupere les parametres
		String id = req.getParameter("id");
		String label = req.getParameter("label");
		String description = req.getParameter("description");
		String ownerid = req.getParameter("owner");
		
		User owner = null;
			try{
				owner = backlogService.getUser(Integer.parseInt(ownerid));
			}
			catch(ClassCastException e){
				logger.error("Erreur lors de la recuperation du user avec son id : " + e.getMessage() );
			}
		
		// On maj le user
		Component composant = null;
			try{
				composant = backlogService.getComponent(Integer.parseInt(id));
			}
			catch(ClassCastException e){
				logger.error("Erreur lors de la recuperation du composant avec son id : " + e.getMessage() );
			}

		composant.setLabel(label);
		composant.setDescription(description);
		composant.setOwner(owner);
		
		backlogService.updateComponent(composant);
		
		
		ModelAndView mNv = new ModelAndView("composantsActionSuccess");
		mNv.addObject("action", "Mise � jour");
		
		return mNv;
	}
	
	@RequestMapping(value="/composants/delete/{id}.do", method=RequestMethod.GET)
	public ModelAndView usersDelete(@PathVariable int id){
			
		// On supprime le composant
		Component composant = null;
			try{
				composant = backlogService.getComponent(id);	
			}
			catch(ClassCastException e){
				logger.error("Erreur lors de la recuperation du composant avec son id : " + e.getMessage() );
			}		

		try{
			backlogService.deleteComponent(composant);	
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la suppression du composant: " + e.getMessage() );
		}
		
		
		ModelAndView mNv = new ModelAndView("composantsActionSuccess");
		mNv.addObject("action", "Suppression");
		
		return mNv;
	}

}
