package edu.flst.backlog.web;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import edu.flst.backlog.bo.Component;
import edu.flst.backlog.bo.Status;
import edu.flst.backlog.bo.Story;
import edu.flst.backlog.bo.User;
import edu.flst.backlog.service.BacklogService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StoriesController {
	
	final static Logger logger = Logger.getLogger(StoriesController.class);
	
	Status status;
	

	@Autowired private BacklogService backlogService;

	@RequestMapping(value="/stories.do", method=RequestMethod.GET)
	public ModelAndView stories(){
		
		List<Story> allStories = new ArrayList<Story>();
		
		try{
			allStories = backlogService.listStories();
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la r�cuperation de la liste des taches (erreur backlogService) : " + e.getMessage() );
		}	
		
		/* On trie les liste par date d�croissante de modification */
		Collections.sort(allStories, new Comparator<Story>() {
		    public int compare(Story s1, Story s2) {
		        return s2.getModifiedDate().compareTo(s1.getModifiedDate());
		    }
		});
		
	    ModelAndView mNv = new ModelAndView("stories");
			mNv.addObject("stories", allStories);

		return mNv;
		
	}
	
	@RequestMapping(value="/stories/new.do", method=RequestMethod.GET)
	public ModelAndView storiesNew(){
		
		List<Component> allComponents 	= new ArrayList<Component>();
		List<User> allUsers 			= new ArrayList<User>();
		
		try{
			allComponents = backlogService.listComponents();
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la r�cuperation de la liste des composants (erreur backlogService) : " + e.getMessage() );
		}
		
		try{
			allUsers = backlogService.listUsers();
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la r�cuperation de la liste des users (erreur backlogService) : " + e.getMessage() );
		}
		
		
		
		ModelAndView mNv = new ModelAndView("storiesNew");
		mNv.addObject("composants", allComponents);	
		mNv.addObject("users", allUsers);
		mNv.addObject("status", Status.values());
		

		return mNv;
		
	}
	   
	@RequestMapping(value="/stories/create.do", method=RequestMethod.POST)
	public ModelAndView storiesCreate(HttpServletRequest req, HttpServletResponse resp){
		
		// On recupere les parametres
		String label 			= req.getParameter("label");
		String commentaire 		= req.getParameter("commentaire");
		String statusString 	= req.getParameter("status");
		String[] users 			= req.getParameterValues("users[]");
		String[] composantsid 	= req.getParameterValues("composants[]");
	
		// On cr�� une nouvelle story
		Story story = new Story();
		story.setLabel(label);
		story.setComment(commentaire);
		story.setStatus(Status.valueOf(statusString));

		for (String userid : users){
			User user = new User();
			try{
				user = backlogService.getUser(Integer.parseInt(userid));
			}
			catch(ClassCastException e){
				logger.error("Erreur lors de la r�cuperation du user id="+userid+" (erreur backlogService) : " + e.getMessage() );
			}
			try{
				story.addUser(user);
			}
			catch(ClassCastException e){
				logger.error("Impossible d'ajouter le user id="+userid+" : " + e.getMessage() );
			}
			
		}
		
		for (String composantid : composantsid){
			Component composant = new Component();
			try{
				composant = backlogService.getComponent(Integer.parseInt(composantid));
			}
			catch(ClassCastException e){
				logger.error("Erreur lors de la r�cuperation du composant id="+composantid+" (erreur backlogService) : " + e.getMessage() );
			}
			try{
				story.addComponent(composant);
			}
			catch(ClassCastException e){
				logger.error("Impossible d'ajouter le composant id="+composantid+" : " + e.getMessage() );
			}
			
		}
		

		try{
			Date createdDate = new Date(); // Date du jour
			story.setCreatedDate(createdDate);
			story.setModifiedDate(createdDate);
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de l'ajout des dates : " + e.getMessage() );
		}
		

		try{
			backlogService.createStory(story);
		}
		catch(ClassCastException e){
			logger.error("Impossible de cr�er la tache : " + e.getMessage() );
		}
		
		ModelAndView mNv = new ModelAndView("storiesActionSuccess");
		mNv.addObject("action", "Cr�ation");
		
		return mNv;
	}
	
	@RequestMapping(value="/stories/edit/{id}.do", method=RequestMethod.GET)
	public ModelAndView storiesEdit(@PathVariable int id){
		
		List<Component> allComponents 	= new ArrayList<Component>();
		List<User> allUsers 			= new ArrayList<User>();
		Story story 					= new Story();
		
		try{
			allComponents = backlogService.listComponents();
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la r�cuperation de la liste des composants (erreur backlogService) : " + e.getMessage() );
		}
		
		try{
			allUsers = backlogService.listUsers();
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la r�cuperation de la liste des users (erreur backlogService) : " + e.getMessage() );
		}
		
		try{
			story = backlogService.getStory(id);
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la r�cuperation de la tache id="+id+" (erreur backlogService) : " + e.getMessage() );
		}
		
		
		ModelAndView mNv = new ModelAndView("storiesEdit");
		mNv.addObject("composants", allComponents);	
		mNv.addObject("users", allUsers);
		mNv.addObject("status", Status.values());
		mNv.addObject("story", story);
		
		return mNv;
	}
	
	@RequestMapping(value="/stories/update.do", method=RequestMethod.POST)
	public ModelAndView storiesUpdate(HttpServletRequest req, HttpServletResponse resp){
		
		// On recupere les parametres
		String label 			= req.getParameter("label");
		String commentaire		= req.getParameter("commentaire");
		String statusString 	= req.getParameter("status");
		String[] users 			= req.getParameterValues("users[]");
		String[] composantsid 	= req.getParameterValues("composants[]");
		String id 				= req.getParameter("id");
	
		Story story 	= new Story();		
		try{
			story 		= backlogService.getStory(Integer.parseInt(id));
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la r�cuperation de la tache id="+id+" (erreur backlogService) : " + e.getMessage() );
		}
		
		story.setLabel(label);
		story.setComment(commentaire);
		story.setStatus(Status.valueOf(statusString));

		//On supprime tous les users
		try{
			Iterator<User> iter = story.getUsers().iterator();
			while(iter.hasNext()) {
			   iter.next();
			   iter.remove();
			}
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la suppression de tous les users de la tache : " + e.getMessage() );
		}
		// On ajoute ceux qui ont �t� s�l�ctionn�s
		try{
			for (String userid : users){
				User user = backlogService.getUser(Integer.parseInt(userid));
				story.addUser(user);
			}
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de l'ajout des users � la tache : " + e.getMessage() );
		}		


		//On supprime tous les composants
		try{
			Iterator<Component> iterComposant = story.getComponents().iterator();
			while(iterComposant.hasNext()) {
				iterComposant.next();
				iterComposant.remove();
			}
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la suppression de tous les composants de la tache : " + e.getMessage() );
		}

		try{
			for (String composantid : composantsid){
				Component composant = backlogService.getComponent(Integer.parseInt(composantid));
				story.addComponent(composant);
			}
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de l'ajout des composants � la tache : " + e.getMessage() );
		}


		try{
			Date modifiedDate = new Date(); // Date du jour
			story.setCreatedDate(story.getCreatedDate());
			story.setModifiedDate(modifiedDate);
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de l'ajout des dates : " + e.getMessage() );
		}
		
		try{
			backlogService.updateStory(story);
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la mise � jour de la tache (erreur backlogService) : " + e.getMessage() );
		}
		

		
		ModelAndView mNv = new ModelAndView("storiesActionSuccess");
		mNv.addObject("action", "Mise � jour");
		
		return mNv;
	}
	
	@RequestMapping(value="/stories/delete/{id}.do", method=RequestMethod.GET)
	public ModelAndView storiesDelete(@PathVariable int id){
			
		// On supprime la tache
		Story story 	= new Story();		
		try{
			story 		= backlogService.getStory(id);	
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la r�cuperation de la tache id="+id+" (erreur backlogService) : " + e.getMessage() );
		}	

		try{
			backlogService.deleteStory(story);	
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la suppression de la tache (erreur backlogService) : " + e.getMessage() );
		}	
		
		
		ModelAndView mNv = new ModelAndView("storiesActionSuccess");
		mNv.addObject("action", "Suppression");
		
		return mNv;
	}
	
	@RequestMapping(value="/stories/lastest.do")
	public void getLatestStories(HttpServletRequest request, HttpServletResponse response) throws IOException{ 
			
		String result = "<center><strong>DERNIERES TACHES : </strong></center><br>";
		
		// On recupere toutes les taches
		List<Story> allStories 		= new ArrayList<Story>();
		List<Story> laststories 	= new ArrayList<Story>();
		try{
			allStories 				= backlogService.listStories();
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la recuperation des taches (erreur backlogService) : " + e.getMessage() );
		}	
		
		
		Collections.sort(allStories, new Comparator<Story>() {
		    public int compare(Story s1, Story s2) {
		        return s2.getModifiedDate().compareTo(s1.getModifiedDate());
		    }
		});
		/* Fin des tris */
	        
		// On garde que les plus recentes
		if(allStories.size() > 3){
			laststories = allStories.subList(0, 3);			
		}
		else{
			laststories = allStories;
		}
		

		/* On construit le tableau pour le widget des dernieres stories de la pages d'accueil */
			result += 	"<table class='table table-condensed table-hover'>";
			result += 		"<tr>";
			result +=			"<th>Label</th>";
			result +=			"<th>Composant</th>";
			result +=			"<th>Utilisateurs</th>";
			result +=			"<th>Status</th>";
			result +=			"<th>Commentaire</th>";
			result +=			"<th></th>";
			result += 		"</tr>";
		if(!(allStories.isEmpty())){								
			for (Story story : laststories){
				result +=	"<tr>";
				result +=		"<td>"+story.getLabel()+"</td>";
				result +=		"<td>";
				for (Component composant : story.getComponents()){
					result +=		"<ul>"+composant.getLabel()+"</ul>";
				}
				result +=		"</td>";
				result +=		"<td>";
				for (User user : story.getUsers()){
					result +=		"<ul>"+user.getFirstName()+" "+user.getLastName()+"</ul>";
				}
				result +=		"</td>";
				result +=		"<td>"+story.getStatus()+"</td>";
				result +=		"<td>"+story.getComment()+"</td>";
				result +=		"<td><a href='./stories/edit/"+story.getId()+".do'><span class='glyphicon glyphicon-zoom-in'></span></a></td>";
				result +=	"</tr>";
			}
			
			result += 	"</table>";
		}
		 
		// On renvoi le html en reponse
		response.setContentType("text/html");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(result);
	}

}
