package edu.flst.backlog.web;
import java.util.ArrayList;
import java.util.List;

import edu.flst.backlog.bo.Job;
import edu.flst.backlog.bo.User;
import edu.flst.backlog.service.BacklogService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UtilisateursController {
	
	final static Logger logger = Logger.getLogger(UtilisateursController.class);
	
	Job job;
	
	@Autowired private BacklogService backlogService;

	@RequestMapping(value="/users.do", method=RequestMethod.GET)
	public ModelAndView users(){
		
		List<User> allUsers 	= new ArrayList<User>();
		
		try{
			allUsers 			= backlogService.listUsers();
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la recuperation des users (erreur backlogService) : " + e.getMessage() );
		}	
		
	    ModelAndView mNv = new ModelAndView("users");
			mNv.addObject("users", allUsers);	


		return mNv;
		
	}
	
	@RequestMapping(value="/users/new.do", method=RequestMethod.GET)
	public ModelAndView usersNew(){
		
		ModelAndView mNv = new ModelAndView("usersNew");
		
		
		mNv.addObject("job", Job.values());

		return mNv;
		
	}
	
	@RequestMapping(value="/users/edit/{id}.do", method=RequestMethod.GET)
	public ModelAndView usersEdit(@PathVariable int id){
		ModelAndView mNv = new ModelAndView("usersEdit");
		
		User user 	= new User() ;
			
		try{
			user 	= backlogService.getUser(id);
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la recuperation du user id="+id+" (erreur backlogService) : " + e.getMessage() );
		}
		
		mNv.addObject("user", user);
		mNv.addObject("job", Job.values());
		
		return mNv;
	}
	   
	@RequestMapping(value="/users/create.do", method=RequestMethod.POST)
	public ModelAndView usersCreate(HttpServletRequest req, HttpServletResponse resp){
		
		// On recupere les parametres
		String firstName 	= req.getParameter("firstName");
		String lastName 	= req.getParameter("lastName");
		String jobString 	= req.getParameter("job");
	
		// On cr�� un nouveau user
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setJob(Job.valueOf(jobString));
		
		try{
			backlogService.createUser(user);
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la cr�ation du user (erreur backlogService) : " + e.getMessage() );
		}
		
		ModelAndView mNv = new ModelAndView("usersActionSuccess");
		mNv.addObject("action", "Cr�ation");
		
		return mNv;
	}
	
	@RequestMapping(value="/users/update.do", method=RequestMethod.POST)
	public ModelAndView usersUpdate(HttpServletRequest req, HttpServletResponse resp){
		
		// On recupere les parametres
		String firstName 	= req.getParameter("firstName");
		String lastName 	= req.getParameter("lastName");
		String jobString 	= req.getParameter("job");
		String id 			= req.getParameter("id");
	
		// On maj le user
		User user	= new User();
		try{
			user 	= backlogService.getUser(Integer.parseInt(id));
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la recuperation du user id="+id+" (erreur backlogService) : " + e.getMessage() );
		}		

		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setJob(Job.valueOf(jobString));
		
		try{
			backlogService.updateUser(user);
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la mise � jour du user (erreur backlogService) : " + e.getMessage() );
		}	
				
		
		ModelAndView mNv = new ModelAndView("usersActionSuccess");
		mNv.addObject("action", "Mise � jour");
		
		return mNv;
	}
	
	@RequestMapping(value="/users/delete/{id}.do", method=RequestMethod.GET)
	public ModelAndView usersDelete(@PathVariable int id){
			
		// On recupere le user
		User user	= new User();
		try{
			user 	= backlogService.getUser(id);	
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la recuperation du user id="+id+" (erreur backlogService) : " + e.getMessage() );
		}	

		// On supprime le user
		try{
			backlogService.deleteUser(user);
		}
		catch(ClassCastException e){
			logger.error("Erreur lors de la suppression du user (erreur backlogService) : " + e.getMessage() );
		}
		
		
		ModelAndView mNv = new ModelAndView("usersActionSuccess");
		mNv.addObject("action", "Suppression");
		
		return mNv;
	}

}
