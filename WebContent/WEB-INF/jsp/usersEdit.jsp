<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Editer utilisateur</title>
	</head>
	<body>
	
		<jsp:include page="menu.jsp" />
	
		<c:set var="req" value="${pageContext.request}" />
		<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
	           
		<div class="alert alert-info col-sm-6 col-sm-offset-2">
	 		<center><strong>Modification de l'utilisateur</strong></center>
		</div>
	
		<div class="col-sm-8">
			<form method="post" action="../update.do">
		 
				<table class="table table-condensed">
			   		<tr><td><strong>Prenom : </strong></td><td><div class="col-sm-6"><input type="text" class="form-control" name="firstName" value="${user.firstName}" data-minlength="2" data-maxlength="30" required></div></td></tr>
			   		<tr><td><strong>Nom : </strong></td><td><div class="col-sm-6"><input type="text" class="form-control" name="lastName" value="${user.lastName}" data-minlength="2" data-maxlength="30" required></div></td></tr>
			   		<tr>
				   		<td><strong>Profession : </strong></td>
				   		<td>
				   			<div class="col-sm-6">
				   				<label>
							   		<select class="pretty-select" name="job">
							   			<option selected value="${user.job}">${user.job}</option>
							            <c:forEach var="j" items="${job}" >
									    	<c:if test="${j ne user.job}">
									     		<option value="${j}">${j}</option>
									      	</c:if>           	
							            </c:forEach>
									</select>
								</label>
							</div>
						</td>
					</tr>		
				 </table>
				 
				 <br>
				 <input type="hidden" name="id" value="${user.id}">
		
				 <div class="form-horizontal">
				 	<input type="submit" class="btn btn-success" value="Editer"/>
					<a href="${baseURL}/./users.do" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>	
				</div>
			</form>
		</div>
	
	</body>
</html>