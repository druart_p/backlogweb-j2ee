 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Nouveau composant</title>
	</head>
	<body>
	
		<jsp:include page="menu.jsp" />
		
		<c:set var="req" value="${pageContext.request}" />
		<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
	           
		<div class="alert alert-info col-sm-6 col-sm-offset-2">
	 		<center><strong>Cr�ation d'un nouveau composant</strong></center>
		</div>
	    
	    <div class="col-sm-8">     
			<form method="post" action="create.do">
				<table class="table table-condensed">
			   		<tr><td><strong>Label : </strong></td><td><div class="col-sm-8"><input type="text" class="form-control" name="label" placeholder="Titre" data-minlength="2" data-maxlength="50" required></div></td></tr>
			   		<tr><td><strong>Description : </strong></td><td><div class="col-sm-8"><textarea class="form-control" rows="4" cols="50" name="description" placeholder="Description"></textarea></div></td></tr>
			   		<tr>
				   		<td><strong>Propri�taire : </strong></td>
				   		<td>
				   			<div class="col-sm-8">
					   			<label>
							   		<select class="pretty-select" name="owner" required>
							            <c:forEach var="u" items="${users}" >
							            	<option value="${u.id}">${u.firstName} ${u.lastName}</option>
							            </c:forEach>
									</select>
								</label>
							</div>
						</td>
					</tr>		
				 </table>
				 
				 <br>
				 <div class="form-horizontal">
				 	<input type="submit" class="btn btn-success" value="Cr�er"/>
					<a href="${baseURL}/./composants.do" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>	
				</div>
			</form>
		</div>
		
	</body>
</html>