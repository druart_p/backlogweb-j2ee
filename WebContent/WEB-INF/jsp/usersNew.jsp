 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Nouvel utilisateur</title>		
	</head>
	<body>
	
		<jsp:include page="menu.jsp" />
		
		<c:set var="req" value="${pageContext.request}" />
		<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
	           
		<div class="alert alert-info col-sm-6 col-sm-offset-2">
	 		<center><strong>Cr�ation d'un nouvel utilisateur</strong></center>
		</div>
		
		<div class="col-sm-8">
			<form id="form1" data-toggle="validator" method="post" action="create.do">
				<table class="table table-condensed">
			   		<tr><td><strong>Prenom : </strong></td><td><div class="col-sm-6"><input type="text" class="form-control" name="firstName" placeholder="Prenom" data-minlength="2" data-maxlength="30" required></div></td></tr>
			   		<tr><td><strong>Nom : </strong></td><td><div class="col-sm-6"><input type="text" data-minlength="2" data-maxlength="30" class="form-control" name="lastName" placeholder="Nom" required></div></td></tr>
			   		<tr>
				   		<td><strong>Profession : </strong></td>
				   		<td>
				   			<div class="col-sm-6">
				   				<label>
							   		<select class="pretty-select" name="job">
							            <c:forEach var="j" items="${job}" >
							            	<option value="${j}">${j}</option>
							            </c:forEach>
									</select>
								</label>
							</div>
						</td>
					</tr>		
				 </table>
				 <br>
				 
				 <div class="form-horizontal">
				 	<input type="submit" class="btn btn-success" value="Cr�er"/>
					<a href="${baseURL}/./users.do" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>	
				</div>
			</form>
		</div>
	</body>
</html>