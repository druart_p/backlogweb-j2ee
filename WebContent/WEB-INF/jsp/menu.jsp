<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<c:set var="req" value="${pageContext.request}" />
	<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />

	<link rel="stylesheet" href="${baseURL}/./resources/css/bootstrap.min.css" />
	<link rel="stylesheet" href="${baseURL}/./resources/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" href="${baseURL}/./resources/css/bootstrap-select.min.css" />
	

  	<link rel="shortcut icon" href="${baseURL}/./resources/img/favicon.ico" type="image/x-icon" />
		
	<script src="${baseURL}/./resources/js/jquery-1.11.2.min.js"></script>	
	<script src="${baseURL}/./resources/js/bootstrap.min.js"></script>	
	<script src="${baseURL}/./resources/js/bootstrap-select.min.js"></script>		
	<script src="${baseURL}/./resources/js/validator.js"></script>
</head>

<body>

	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="${baseURL}/./home.do"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
	    </div>
	

	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><strong>Utilisateurs </strong><span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="${baseURL}/./users.do">Tous les utilisateurs</a></li>
	            <li><a href="${baseURL}/./users/new.do">Ajouter un utilisateur</a></li>
	          </ul>
	        </li>	        
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><strong>Composants </strong><span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="${baseURL}/./composants.do">Tous les composants</a></li>
	            <li><a href="${baseURL}/./composants/new.do">Ajouter un composant</a></li>
	          </ul>
	        </li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><strong>T�ches </strong><span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="${baseURL}/./stories.do">Toutes les t�ches</a></li>
	            <li><a href="${baseURL}/./stories/new.do">Ajouter une t�che</a></li>
	          </ul>
	        </li>	   
	      </ul>  
          <ul class="nav navbar-nav navbar-right">
          	<form class="navbar-form navbar-left" role="search">
          		<div class="form-group">
          			<a id="createDonnees" href="${baseURL}/./demo.do" class="btn btn-primary"><span class="glyphicon glyphicon-import"></span> DEMO</a>
 		 		</div>
 		 	</form>
 		  </ul>  
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<script>
	
		$(function(){
		    $(".dropdown-toggle").click(function(){
		        $(this).dropdown('toggle');
		        return false;
		    });
		});
		
		$(function(){
			$('select').selectpicker();
		});
		
		$(function () {
			$('form').validator();
		});		
	</script>

</body>
</html>