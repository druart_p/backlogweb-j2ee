<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${action } r�ussie</title>
</head>
<body>

<jsp:include page="menu.jsp" />

	<c:set var="req" value="${pageContext.request}" />
	<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />

	<div class="alert alert-success col-sm-6 col-sm-offset-2">
 		<span class="glyphicon glyphicon-ok"></span>
 		<strong>${action } r�ussie !</strong>
	</div>

	<div class="col-sm-8">
		<form class="form-horizontal">
			<a href="${baseURL}/./composants.do" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>
			
			<a href="${baseURL}/./composants/new.do" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Cr�er nouveau</a>
		</form>
	</div>

</body>
</html>