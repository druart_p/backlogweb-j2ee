 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Nouvelle tache</title>
	</head>
	<body>
	           
	   <jsp:include page="menu.jsp" />
	   
		<c:set var="req" value="${pageContext.request}" />
		<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
	           
		<div class="alert alert-info col-sm-6 col-sm-offset-2">
	 		<center><strong>Cr�ation d'une nouvelle t�che</strong></center>
		</div>
		
		<div class="col-sm-8">	
			<form method="post" action="create.do">
				<table class="table table-condensed">
			   		<tr><td><strong>Label : </strong></td><td><div class="col-sm-8"><input type="text" class="form-control" name="label" placeholder="Titre" data-minlength="2" data-maxlength="50" required></div></td></tr>
			   		<tr><td><strong>Commentaire : </strong></td><td><div class="col-sm-8"><textarea class="form-control" rows="2" cols="50" name="commentaire" placeholder="Commentaire"></textarea></div></td></tr>
			   		<tr>
				   		<td><strong>Utilisateurs : </strong></td>
				   		<td>
				   			<div class="col-sm-8">
					   			<label>
							   		<select name="users[]" required multiple data-live-search="true" data-size="auto">
							            <c:forEach var="u" items="${users}" >
							            	<option value="${u.id}">${u.firstName} ${u.lastName}</option>
							            </c:forEach>
									</select>
								</label>
							</div>
						</td>
					</tr>	
			   		<tr>
				   		<td><strong>Status : </strong></td>
				   		<td>
				   			<div class="col-sm-8">
						   		<select name="status" required>
						            <c:forEach var="s" items="${status}" >
						            	<option value="${s}">${s}</option>
						            </c:forEach>
								</select>
							</div>
						</td>
					</tr>
			   		<tr>
				   		<td><strong>Composants : </strong></td>
				   		<td>
				   			<div class="col-sm-8">
						   		<select name="composants[]" required multiple data-live-search="true" data-size="auto">
						            <c:forEach var="c" items="${composants}" >
						            	<option value="${c.id}">${c.label}</option>
						            </c:forEach>
								</select>
							</div>
						</td>
					</tr>					
				 </table>
				 
				 <br>
				 <div class="form-horizontal">
				 	<input type="submit" class="btn btn-success" value="Cr�er"/>
					<a href="${baseURL}/./stories.do" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>	
				 </div>
				
			</form>
		</div>
	
		
	</body>
</html>