<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Editer t�che</title>
	</head>
	<body>
	
		<jsp:include page="menu.jsp" />
	
			<c:set var="req" value="${pageContext.request}" />
			<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
		           
			<div class="alert alert-info col-sm-6 col-sm-offset-2">
		 		<center><strong>Modification de la t�che</strong></center>
			</div>
		<div class="col-sm-8">	
		
			<form method="post" action="../update.do">
			
				<table class="table table-condensed">
			   		<tr><td><strong>Label : </strong></td><td><div class="col-sm-8"><input type="text" class="form-control" name="label" value="${story.label}" data-minlength="2" data-maxlength="50" required></div></td></tr>
			   		<tr><td><strong>Commentaire : </strong></td><td><div class="col-sm-8"><textarea class="form-control" rows="2" cols="50" name="commentaire">${story.comment}</textarea></div></td></tr>
			   		<tr>
				   		<td><strong>Utilisateurs : </strong></td>
				   		<td>
				   			<div class="col-sm-8">
								<select name="users[]" required multiple data-live-search="true" data-size="auto">
									<c:forEach var="u" items="${users}">
										<c:set var="contains" value="false" />
										<c:forEach var="user" items="${story.users}">
										  <c:if test="${user.id eq u.id}">
											<c:set var="contains" value="true" />
										  </c:if>
										</c:forEach>
										<c:if test="${contains == 'true'}">
											<option selected value="${u.id}">${u.firstName} ${u.lastName}</option>
										</c:if>
										<c:if test="${contains == 'false'}">
											<option value="${u.id}">${u.firstName} ${u.lastName}</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
						</td>
					</tr>		   		
			   		<tr>
				   		<td><strong>Status : </strong></td>
				   		<td>
				   			<div class="col-sm-8">
						   		<select name="status" required>
						   			<option selected value="${story.status}">${story.status}</option>
						            <c:forEach var="s" items="${status}" >
								    	<c:if test="${s ne story.status}">
								     		<option value="${s}">${s}</option>
								      	</c:if>
						            </c:forEach>
								</select>
							</div>
						</td>
					</tr>
			   		<tr>
				   		<td><strong>Composants : </strong></td>
				   		<td>
				   			<div class="col-sm-8">
								<select name="composants[]" required multiple data-live-search="true" data-size="auto">
									<c:forEach var="c" items="${composants}">
										<c:set var="containsComponent" value="false" />
										<c:forEach var="comp" items="${story.components}">
										  <c:if test="${comp.id eq c.id}">
											<c:set var="containsComponent" value="true" />
										  </c:if>
										</c:forEach>
										<c:if test="${containsComponent == 'true'}">
											<option selected value="${c.id}">${c.label}</option>
										</c:if>
										<c:if test="${containsComponent == 'false'}">
											<option value="${c.id}">${c.label}</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
						</td>
					</tr>	   			
				 </table>
				 
				 <br>
				 <input type="hidden" name="id" value="${story.id}">
				 
				<div class="form-horizontal">
				 	<input type="submit" class="btn btn-success" value="Editer"/>
					<a href="${baseURL}/./stories.do" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>	
				</div>
			</form>
		</div>
	
	</body>
</html>