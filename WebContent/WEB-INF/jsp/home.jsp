<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		
		<c:set var="req" value="${pageContext.request}" />
		<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
	
		<title>Accueil</title>	
		<link rel="stylesheet" href="${baseURL}/./resources/css/home.css" />
		
	</head>
	<body>
	
	<jsp:include page="menu.jsp" />
	
	<%-- Si on a fait la demo juste avant, on affiche la div d'information --%>
		<c:if test="${fromDemo == 'true'}">
			<div id="alert-demoSuccess" class="alert alert-success  alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  Cr�ation des donn�es avec succ�s !
			</div>
		</c:if>
	
		<div id="div-backlog" class="row-fluid">
			<table class="table table-bordered">
				<tr>
					<th class="backlog"><center>TO DO</center></th>
					<th class="backlog"><center>IN PROGRESS</center></th>
					<th class="backlog"><center>DONE</center></th>
				</tr>
				<tr>
					<td colspan="3">
						<div class="progress">
						  <div class="progress-bar" role="progressbar" aria-valuenow="${pourcentageAccompli}" aria-valuemin="0" aria-valuemax="100" style="width: ${pourcentageAccompli}%;">
						    ${pourcentageAccompli}%
						  </div>
						</div>
					</td>
				</tr>
				<tr>
						<td>
							<c:forEach var="openstory" items="${openStories}" >
								<a href="#">
									<div class="alert alert-backlog alert-danger col-sm-5" id="${openstory.id}" data-toggle="popover" data-trigger="hover" title="Commentaire" data-content="${openstory.comment}" data-container="body" data-placement="right">
										<center><u><strong>${openstory.label}</strong></u></center>
										<br>
										<strong>Cr�ation : </strong> 
										<br><fmt:formatDate value="${openstory.createdDate}" pattern="dd/MM/yyyy - HH:mm:ss" />
										<br>
										<strong>Derni�re modif. : </strong> 
										<br><fmt:formatDate value="${openstory.modifiedDate}" pattern="dd/MM/yyyy - HH:mm:ss" />
										<br><br>
										<strong>Utilisateur(s) : </strong> 
											<c:forEach var="u" items="${openstory.users}" >
												<ul>${u.firstName} ${u.lastName}</ul>
											</c:forEach>
										<br>
										<strong>Composant(s) : </strong> 
											<c:forEach var="c" items="${openstory.components}" >
												<ul>${c.label}</ul>
											</c:forEach>
									</div>
								</a>
							 </c:forEach>
						 </td>
					 
					 	<td>
							 <c:forEach var="progress" items="${inProgressStories}" >
							 	<a href="#">
									<div class="alert alert-backlog alert-warning col-sm-5" id="${progress.id}" data-toggle="popover" data-trigger="hover" title="Commentaire" data-content="${progress.comment}" data-container="body" data-placement="right">
										<center><u><strong>${progress.label}</strong></u></center>
										<br>
										<strong>Cr�ation : </strong> 
										<br><fmt:formatDate value="${progress.createdDate}" pattern="dd/MM/yyyy - HH:mm:ss" />
										<br>
										<strong>Derni�re modif. : </strong> 
										<br><fmt:formatDate value="${progress.modifiedDate}" pattern="dd/MM/yyyy - HH:mm:ss" />
										<br><br>
										<strong>Utilisateur(s) : </strong> 
											<c:forEach var="u" items="${progress.users}" >
												<ul>${u.firstName} ${u.lastName}</ul>
											</c:forEach>
										<br>
										<strong>Composant(s) : </strong> 
											<c:forEach var="c" items="${progress.components}" >
												<ul>${c.label}</ul>
											</c:forEach>
									</div>
								</a>
							 </c:forEach>
						 </td>
						 
						 <td>
							 <c:forEach var="donestory" items="${doneStories}" >
							 	<a href="#">
									<div class="alert alert-backlog alert-success col-sm-5" id="${donestory.id}" data-toggle="popover" data-trigger="hover" title="Commentaire" data-content="${donestory.comment}" data-container="body" data-placement="left">
										<center><u><strong>${donestory.label}</strong></u></center>
										<br>
										<strong>Cr�ation : </strong> 
										<br><fmt:formatDate value="${donestory.createdDate}" pattern="dd/MM/yyyy - HH:mm:ss" />
										<br>
										<strong>Derni�re modif. : </strong> 
										<br><fmt:formatDate value="${donestory.modifiedDate}" pattern="dd/MM/yyyy - HH:mm:ss" />
										<br><br>
										<strong>Utilisateur(s) : </strong> 
											<c:forEach var="u" items="${donestory.users}" >
												<ul>${u.firstName} ${u.lastName}</ul>
											</c:forEach>
										<br>
										<strong>Composant(s) : </strong> 
											<c:forEach var="c" items="${donestory.components}" >
												<ul>${c.label}</ul>
											</c:forEach>
									</div>
								</a>
							 </c:forEach>
						 </td>			
				</tr>
			</table>
		</div>
	
	<br><br>	

		<center>
			<div id="lateststories" class="alert alert-info col-sm-10 col-sm-offset-1">
		 		<center><strong>DERNIERES TACHES : </strong></center><br>
				<table class='table table-condensed table-hover'>
					<tr>		 		
				 		<th>Label</th>
						<th>Composants</th>
						<th>Utilisateurs</th>
						<th>Status</th>
						<th>Commentaire</th>
						<th></th>
					</tr>
					
		 			<c:forEach var="story" items="${lastStories}" >
						<tr>
							<td>${story.label}</td>
							<td>
								<c:forEach var="c" items="${story.components}" >
									<ul>${c.label}</ul>
								</c:forEach>
							</td>
							<td>
								<c:forEach var="u" items="${story.users}" >
									<ul>${u.firstName} ${u.lastName}</ul>
								</c:forEach>
							</td>
							<td>${story.status}</td>
							<td>${story.comment}</td>
							<td><a href='${baseURL}/./stories/edit/${story.id}.do'><span class='glyphicon glyphicon-zoom-in'></span></a></td>
						</tr>
				 	</c:forEach>	
				</table>
			</div>
		</center>
		
				 
		<script>
			$(document).ready(function() {
				
				var fromDemo = "${fromDemo}";
				
				if(fromDemo == "true"){
					$("#createDonnees").attr("disabled", true);
					
					$("#alert-demoSuccess").delay(4000).slideUp(500, function() {
					    $("#alert-demoSuccess").alert('close');
					});
				}
			
				$("#div-backlog").on("click", ".alert-backlog", function() {	
					window.location.href = "stories/edit/"+$(this).attr("id")+".do";
				});
								
	
				window.setInterval(function(){
					$.ajax({type:"GET",            
						data: {}, 
						url:"stories/lastest.do",
						success: function(result){
							if(result != ""){
								$("#lateststories").html(result);
							}
							else{
								$("#lateststories").html("Error widget 'Dernieres taches'");
							}							
						},
					});
					
					$.ajax({type:"GET",            
						data: {}, 
						url:"home/majdiv.do",
						success: function(result){
							if(result != ""){
								$('#div-backlog [data-toggle="popover"]').popover('destroy');
								$("#div-backlog").html(result);
								$('#div-backlog [data-toggle="popover"]').popover({ html : true });
							}
							else{
								$("#div-backlog").html("Error ajax");
							}							
						},
					});
					
				}, 1000); // Toutes les 1 sec
				
			});

		</script>
	</body>
</html>