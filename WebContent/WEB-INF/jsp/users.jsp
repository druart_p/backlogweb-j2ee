<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Utilisateurs</title>
	</head>
	<body>
	
		<jsp:include page="menu.jsp" />
		
		<div class="col-sm-2 col-sm-offset-1">
			<a href="users/new.do" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Ajouter</a>
		</div>
		<div class="alert alert-info col-sm-6">
			<center><strong>Liste des utilisateurs</strong></center>
		</div>
		
		<br><br>
		
		<table class="table table-condensed table-hover">
			<tr>
				<th>Prenom</th>
				<th>Nom</th>
				<th>Profession</th>
				<th colspan="2">Actions</th>
			</tr>
			<c:forEach var="u" items="${users}" >
				<tr>
					<td>${u.firstName}</td>
					<td>${u.lastName}</td>
					<td>${u.job}</td>
					<td>
						<a href="users/edit/${u.id}.do" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-pencil"></span> Modifier</a>
						<a href="users/delete/${u.id}.do" class="btn btn-danger btn-sm col-sm-offset-1"><span class="glyphicon glyphicon-trash"></span> Supprimer</a>
					</td>
				</tr>
			 </c:forEach>
		</table>	
		
	</body>
</html>