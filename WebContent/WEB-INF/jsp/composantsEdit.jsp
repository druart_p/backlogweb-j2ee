<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Editer composant</title>
	</head>
	
	<body>
	
		<jsp:include page="menu.jsp" />
	
		<c:set var="req" value="${pageContext.request}" />
		<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
	           
		<div class="alert alert-info col-sm-6 col-sm-offset-2">
	 		<center><strong>Modification de l'utilisateur</strong></center>
		</div>
	
		<div class="col-sm-8">	
			<form method="post" action="../update.do">
			
				<table class="table table-condensed">
			   		<tr><td><strong>Label : </strong></td><td><div class="col-sm-8"><input type="text" class="form-control" name="label" value="${composant.label}" data-minlength="2" data-maxlength="50" required></div></td></tr>
			   		<tr><td><strong>Description : </strong></td><td><div class="col-sm-8"><textarea class="form-control" rows="4" cols="50" name="description">${composant.description}</textarea></div></td></tr>
			   		<tr>
				   		<td>Propriétaire : </td>
				   		<td>
				   			<div class="col-sm-8">
					   			<label>
							   		<select class="pretty-select" name="owner" required>
							   			<option selected value="${composant.owner.id}">${composant.owner.firstName} ${composant.owner.lastName}</option>
							            <c:forEach var="u" items="${users}" >
									    	<c:if test="${u.id != composant.owner.id}">
									     		<option value="${u.id}">${u.firstName} ${u.lastName}</option>
									      	</c:if>           	
							            </c:forEach>
									</select>
								</label>
							</div>
						</td>
					</tr>		   			
				 </table>
				 
				 <br>
				 <input type="hidden" name="id" value="${composant.id}">
		
				 <div class="form-horizontal">
				 	<input type="submit" class="btn btn-success" value="Editer"/>
					<a href="${baseURL}/./composants.do" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>	
				</div>
			</form>
		</div>
		
	</body>
</html>