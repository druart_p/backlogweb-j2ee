<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Taches</title>
	</head>
	<body>
	
		<jsp:include page="menu.jsp" />
		
		<div class="col-sm-2 col-sm-offset-1">
			<a href="stories/new.do" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Ajouter</a>
		</div>
		<div class="alert alert-info col-sm-6">
			<center><strong>Liste des t�ches</strong></center>
		</div>
		
		<br><br>
		
		<table class="table table-condensed table-hover">
			<tr>
				<th>Label</th>
				<th>Commentaire</th>
				<th>Date de cr�ation</th>
				<th>Derni�re modification</th>
				<th>Composants</th>
				<th>Utilisateurs</th>
				<th>Status</th>
				<th colspan="2">Actions</th>
			</tr>
			<c:forEach var="s" items="${stories}" >
				<tr>
					<td>${s.label}</td>
					<td>${s.comment}</td>
					<td><fmt:formatDate value="${s.createdDate}" pattern="dd/MM/yyyy - HH:mm:ss" /></td>
					<td><fmt:formatDate value="${s.modifiedDate}" pattern="dd/MM/yyyy - HH:mm:ss" /></td></td>
					<td>
						<c:forEach var="c" items="${s.components}" >
							<ul>${c.label}</ul>
						</c:forEach>					
					</td>
					<td>
						<c:forEach var="u" items="${s.users}" >
							<ul>${u.firstName} ${u.lastName}</ul>
						</c:forEach>
					</td>
					<td>${s.status}</td>
					<td>
						<a href="stories/edit/${s.id}.do" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-pencil"></span> Modifier</a>
						<a href="stories/delete/${s.id}.do" class="btn btn-danger btn-sm col-sm-offset-1"><span class="glyphicon glyphicon-trash"></span> Supprimer</a>
					</td>
				</tr>
			 </c:forEach>
		</table>	
		
	</body>
</html>